/**************************************************
 * This is a parser to be used for a connect four
 * game to get command line arguments.
 *
 * @author Kyle Hekhuis
 *************************************************/

#ifndef H_CONNECT_PARSER
#define H_CONNECT_PARSER

/******************************************************
 * Struct holding amount of arguments this program can
 * take as well as the variable types of arguments
 * that it takes.
 *****************************************************/
struct arguments {
	char *args[5];
	int width, height, square, connect;
	char *loadFile;
};

/***************************************
 * Parses a single command line option.
 **************************************/
error_t parse_opt(int key, char *arg, struct argp_state *state);

/********************************
 * Command line argument parser. 
 *******************************/
struct argp argp;

/******************************************************
 * Instance of arguments struct to hold the arguments.
 *****************************************************/
struct arguments args;

/****************************************
 * Sets up variables for argument struct 
 * with passed command line arguments.
 ***************************************/
void setup(int argc, char** argv);

/**************************************************************
 * Checks to see if passed parameters are valid or not. Gives
 * error and exits program if one of the parameters is wrong.
 *
 * @param width width param to check
 * @param height height param to check
 * @param square square param to check
 * @param connect connect param to check
 *************************************************************/
void errorCheck(int width, int height, int square, int connect);

#endif