/******************************************************
 * Connect Four is a program created for CIS 343:
 * Structure of Programming Languages at Grand Valley
 * State University during the Winter 2017 semester.
 * In this program you play a game of Connect Four
 * (https://en.wikipedia.org/wiki/Connect_Four)
 * except the user can define the width and height
 * of the board as well as the amount needed to
 * connect in a row to win. Designed for Linux.
 *
 *@author Kyle Hekhuis
 *****************************************************/
 
#define _GNU_SOURCE //getline() won't get compiled without this
#include <stdio.h>
#include <stdlib.h>
#include <argp.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <signal.h>
#include <limits.h>
#include "file_utils.h"
#include "connect_parser.h"

/********************************************************
 * Graphics to apply to print outs.
 * Based off example on terminal colors on StackOverflow
 * http://stackoverflow.com/a/23657072
 * and ANSI Escape Sequences
 * http://ascii-table.com/ansi-escape-sequences.php
 *******************************************************/
#define RED		"\x1B[31m"
#define CYAN	"\x1B[36m"
#define MAGENTA	"\x1B[35m"
#define RESET	"\x1B[0m"
#define BOLD	"\x1B[1m"

/*************************************************
 * Struct holding the game state which is the
 * width, height, connect amount, current player,
 * dynamic 2D array board, game over status, and
 * player scores.
 ************************************************/
struct GameState {
	//Width of board
	int width;
	//Height of board
	int height;
	//Amount to connect in row for win
	int connect;
	//Current player's turn
	int currentPlayer;
	//Dynamic array of game board
	char **board;
	//Check if board has been allocated
	bool allocated;
	//Check if the game is over
	bool gameOver;
	//Keep score through multiple games
	int score[2];
};

/** Colored Player 1 string. */
static const char* P1 = CYAN "Player 1" RESET;
/** Colored Player 2 string. */
static const char* P2 = MAGENTA "Player 2" RESET;

/** Usage instructions while in the game. */
static const char *INGAME_HELP = "\nPlace a piece by entering the column number.\n" 
								 CYAN "Player 1" RESET " is " CYAN "'X'" RESET 
								 " and " MAGENTA "Player 2" RESET " is " MAGENTA 
								 "'O'" RESET ".\nYou can save or load at anytime by"
								 " entering '-s filename' to save and '-l filename' "
								 "to load. To quit, type '-q'. For help, type '-h'.\n";

void playGame(struct GameState* gs, bool loaded);
void newGame(struct GameState* gs);
bool placePiece(int col, struct GameState* gs);
void checkWin(struct GameState* gs, int col, int row);
bool checkVertical(struct GameState* gs, int col, int row, char player);
bool checkHorizontal(struct GameState* gs, int col, int row, char player);
bool checkDiagonal(struct GameState* gs, int col, int row, char player);
void nextPlayer(struct GameState* gs);
void printBoard(struct GameState* gs);
void clearScreen();
void save(struct GameState* gs, char *saveFile);
char* intToStr(int x, int* sizeTotal);
void load(struct GameState* gs, char *loadFile);
void loadErrorHandler();

/*************************************
 * Takes in command line arguments 
 * and starts the connect four game.
 ************************************/
int main(int argc, char** argv) {
	//Holds the entire game state
	struct GameState gs;
	gs.allocated = false;
	
	setup(argc, argv);
	//Check if load file passed
	if (args.loadFile != NULL) {
		load(&gs, args.loadFile);
		playGame(&gs, true);
	} else {
		gs.width = args.width;
		gs.height = args.height;
		gs.connect = args.connect;
		gs.currentPlayer = 1;
		gs.score[0] = 0;
		gs.score[1] = 0;
		playGame(&gs, false);
	}

	free(gs.board);
	return 0;
}

/*****************************************************
 * Runs the game with while loop and gets user input.
 *
 * @param gs struct containing the game state
 * @param loaded boolean for if command line arg to
 *				 load game was passed.
 ****************************************************/
void playGame(struct GameState* gs, bool loaded) {
	clearScreen();
	if (loaded) {
		printf(BOLD "Game loaded successfully!\n" RESET);
	} else {
		newGame(gs);
	}
	//Max size of line input from limits.h
	size_t inputSize = _POSIX2_LINE_MAX;
	char* input; //Hold input data from user
	input = malloc(inputSize * sizeof(char));
	if (input == NULL) {
		printf(RED "Failure to allocate memory for user input.\n" RESET);
		exit(1);
	}
	int col;
	while (true) {
		printf(BOLD CYAN "CONNECT" MAGENTA " 'FOUR'!\n" RESET "===============");
		printf("%s\n", INGAME_HELP);
		printf("Score is:\n%s: %d\n%s: %d\n", 
			   P1, gs->score[0], P2, gs->score[1]);
		while (!gs->gameOver) {
			printBoard(gs);
			printf("\nIt is %s's turn\n", (gs->currentPlayer == 1) ? P1 : P2);
			//Get user input
			getline(&input, &inputSize, stdin);
			//Remove the '\n' from input
			input[strcspn(input, "\n")] = '\0';
			//Check for load command
			if (strncmp(input, "-l ", 3) == 0) {
				load(gs, (input + 3));
				clearScreen();
				printf(BOLD "Game loaded successfully!\n" RESET);
				printf("%s\n", INGAME_HELP);
				printf("Score is:\n%s: %d\n%s: %d\n", 
					   P1, gs->score[0], P2, gs->score[1]);
			}
			//Check for save command
			else if (strncmp(input, "-s ", 3) == 0) {
				save(gs, (input + 3));
			}
			//Check for quit command
			else if (strcmp(input, "-q") == 0) {
				return;
			}
			//Check for help command
			else if (strcmp(input, "-h") == 0) {
				printf("%s", INGAME_HELP);
			}
			//Get col to place piece and check if valid
			else if ((col = atoi(input)) > 0 && col <= gs->width) {
				if (placePiece(col, gs)) {
					nextPlayer(gs);
				}
			}
			//Invalid input if nothing else hit
			else {
				printf(RED "\aInvalid input! Type -h for help.\n" RESET);
			}
		}
		printf("\nWould you like to play another game?\n"
			   "Enter '-y' for yes, anything else for no.\n");
		//Get user input
		getline(&input, &inputSize, stdin);
		//Remove the '\n' from input
		input[strcspn(input, "\n")] = '\0';
		if (strcmp(input, "-y") == 0) {
			newGame(gs);
			gs->currentPlayer = 1; //Reset to first player
			//Clear screen so user doesn't get confused seeing
			//previous game print outs
			clearScreen();
		} else {
			return;
		}
	}
	
	free(input);
}

/***********************************************************
 * Creates a new game with a board of size width * height
 * and connect in a row amount to win equal to connect var.
 * The current player is the starting player. Free's the
 * board if it's already allocated.
 *
 * @param gs struct containing the game state
 **********************************************************/
void newGame(struct GameState* gs) {
	gs->gameOver = false;
	//Free already allocated board if exist
	if (gs->allocated) {
		free(gs->board);
	} else {
		gs->allocated = true;
	}
	//Allocate memory for the board
	gs->board = malloc(sizeof(char *) * gs->width);
	if (gs->board == NULL) {
		printf(RED "Failure to allocate memory for board.\n" RESET);
		exit(1);
	}
	for (int i = 0; i < gs->width; i++) {
		gs->board[i] = malloc(sizeof(char) * gs->height);
		if (gs->board[i] == NULL) {
		printf(RED "Failure to allocate memory for board.\n" RESET);
		exit(1);
		}
	}
	//Create empty board
	for (int i = 0; i < gs->height; i++) {
		for (int j = 0; j < gs->width; j++) {
			gs->board[j][i] = '*';
		}
	}
}

/*******************************************************
 * Places a piece in the specified column position on
 * the board for the current player.
 *
 * @param col column to place piece
 * @param gs struct containing the game state
 * @return true if piece placed successfully,
 * 		   false if not
 ******************************************************/
bool placePiece(int col, struct GameState* gs) {
	col--; //To line up with array since array starts at 0.
	for (int i = gs->height - 1; i >= 0; i--) {
		if (gs->board[col][i] == '*') {
			if (gs->currentPlayer == 1) {
				gs->board[col][i] = 'X';
			} else {
				gs->board[col][i] = 'O';
			}
			checkWin(gs, col, i);
			return true;
		}
	}
	printf(RED "\aCan't place piece there! Try again!\n" RESET);
	return false;
}

/****************************************************
 * Checks to see if winning conditions have been met
 * in the game by calling the functions to check
 * vertical, horizontal, and diagonal wins. Also
 * checks for draw by seeing if top row on board is
 * full with no wins.
 *
 * @param gs struct containing the game state
 * @param col column the piece was placed in
 * @param row row the piece was placed in
 ***************************************************/
void checkWin(struct GameState* gs, int col, int row) {
	char player;
	if (gs->currentPlayer == 1) {
		player = 'X';
	} else {
		player = 'O';
	}
	
	if (checkVertical(gs, col, row, player) ||
		checkHorizontal(gs, col, row, player) ||
		checkDiagonal(gs, col, row, player)) {
		
		printBoard(gs);
		printf(BOLD "\nPlayer %d has won!\n" RESET, gs->currentPlayer);
		gs->gameOver = true;
		gs->score[gs->currentPlayer - 1]++;
	}
	//Check for draw by checking that top row is full
	//and game hasn't been won
	if (!gs->gameOver) {
		int amountLeft = 0;
		for (int i = 0; i < gs->width; i++) {
			if (gs->board[i][0] == '*') {
				amountLeft++;
			}
		}
		if (amountLeft == 0) {
			gs->gameOver = true;
			printBoard(gs);
			printf(BOLD "\nDraw! All spots occupied and no winner!\n" RESET);
		}
	}
}

/****************************************************
 * Checks to see if winning conditions have been met
 * in the vertical direction of placed piece. Does
 * so by checking if there are similar pieces below
 * the placed piece and incrementing a counter for
 * each time there is. If the counter is equal to
 * the amount needed to connect in a row for a win,
 * it returns true.
 *
 * @param gs struct containing the game state
 * @param col column the piece was placed in
 * @param row row the piece was placed in
 * @param player char representation of player who
 *				 placed piece
 * @return true if win has been found vertically
 ***************************************************/
bool checkVertical(struct GameState* gs, int col, int row, char player) {
	//Impossible for vertical win to happen if connect 
	//is greater than the height dimension.
	if (gs->connect > gs->height) {
		return false;
	}
	
	//Set count to 1 since starting point counts as 1
	int count = 1;
	
	//Checks below the placed piece on the board.
	//Don't have to check above since impossible to
	//have that in this game.
	for (int i = row + 1; i < gs->height; i++) {
		if (gs->board[col][i] == player) {
			count++;
		} else {
			break;
		}
	}
	//If count is equal to amount needed to connect in
	//a row return true for a win.
	if (count == gs->connect) {
		return true;
	} else {
		return false;
	}
}

/****************************************************
 * Checks to see if winning conditions have been met
 * in the horizontal direction of placed piece. Does
 * so by checking if there are similar pieces on
 * either side of the placed piece and incrementing a
 * counter for each time there is. If the counter is 
 * equal to the amount needed to connect in a row for
 * a win, it returns true.
 *
 * @param gs struct containing the game state
 * @param col column the piece was placed in
 * @param row row the piece was placed in
 * @param player char representation of player who
 *				 placed piece
 * @return true if win has been found horizontally
 ***************************************************/
bool checkHorizontal(struct GameState* gs, int col, int row, char player) {
	//Impossible for horizontal win to happen if connect 
	//is greater than the width dimension.
	if (gs->connect > gs->width) {
		return false;
	}
	//Set count to 1 since starting point counts as 1
	int count = 1;
	
	//Check for matching pieces to the left of starting point
	for (int i = col - 1; i >= 0; i--) {
		if (gs->board[i][row] == player) {
			count++;
		} else {
			break;
		}
	}
	//Check for matching pieces to the right of starting point
	for (int i = col + 1; i < gs->width; i++) {
		if (gs->board[i][row] == player) {
			count++;
		} else {
			break;
		}
	}
	//If count is equal to amount needed to connect in
	//a row return true for a win.
	if (count == gs->connect) {
		return true;
	} else {
		return false;
	}
}

/****************************************************
 * Checks to see if winning conditions have been met
 * in the diagonal direction of placed piece. Does
 * so by checking if there are similar pieces kitty
 * corner to the placed piece and incrementing a 
 * counter for each time there is. If the counter is
 * equal to the amount needed to connect in a row for
 * a win, it returns true.
 *
 * @param gs struct containing the game state
 * @param col column the piece was placed in
 * @param row row the piece was placed in
 * @param player char representation of player who
 *				 placed piece
 * @return true if win has been found diagonally
 ***************************************************/
bool checkDiagonal(struct GameState* gs, int col, int row, char player) {
	//Impossible for diagonal win to happen if connect
	//is greater than the min dimension.
	if (gs->connect > gs->width || gs->connect > gs->height) {
		return false;
	}
	
	int count, i, j;
	
	//Check the / diagonal
	//========================
	//Set count to 1 since starting point counts as 1
	count = 1;
	//Check southwest of point
	i = row + 1;
	j = col - 1;
	while (i < gs->height && j >= 0) {
		if (gs->board[j][i] == player) {
			count++;
		} else {
			break;
		}
		i++;
		j--;
	}
	
	//Check northeast of point
	i = row - 1;
	j = col + 1;
	while (i >= 0 && j < gs->width) {
		if (gs->board[j][i] == player) {
			count++;
		} else {
			break;
		}
		i--;
		j++;
	}
	//If count is equal to amount needed to connect in
	//a row return true for a win.
	if (count == gs->connect) {
		return true;
	}
	
	//Check the \ diagonal
	//========================
	//Set count to 1 since starting point counts as 1
	count = 1;
	//Check southeast of point
	i = row + 1;
	j = col + 1;
	while (i < gs->height && j < gs->width) {
		if (gs->board[j][i] == player) {
			count++;
		} else {
			break;
		}
		i++;
		j++;
	}
	
	//Check northwest of point
	i = row - 1;
	j = col - 1;
	while (i >= 0 && j >= 0) {
		if (gs->board[j][i] == player) {
			count++;
		} else {
			break;
		}
		i--;
		j--;
	}
	//If count is equal to amount needed to connect in
	//a row return true for a win.
	if (count == gs->connect) {
		return true;
	}
	return false;
}

/**********************************************
 * Advances the current player to
 * the next player.
 *
 * @param gs struct containing the game state
 *********************************************/
void nextPlayer(struct GameState* gs) {
	if (gs->currentPlayer == 1) {
		gs->currentPlayer = 2;
	} else {
		gs->currentPlayer = 1;
	}
}

/**********************************************
 * Prints the state of the game board.
 * 'X' represent player 1's pieces and
 * 'O' represent player 2's pieces.
 *
 * @param gs struct containing the game state
 *********************************************/
void printBoard(struct GameState* gs) {
	printf("\n");
	for (int i = 0; i < gs->height; i++) {
		for (int j = 0; j < gs->width; j++) {
			if (gs->board[j][i] == 'X') {
				//Print 'X's in cyan
				printf(CYAN "%c" RESET, gs->board[j][i]);
			} else if (gs->board[j][i] == 'O'){
				//Print 'O's in magenta
				printf(MAGENTA "%c" RESET, gs->board[j][i]);
			} else {
				printf("%c", gs->board[j][i]);
			}
			
		}
		printf("\n");
	}
}

/*********************************************
 * Method created by Professor Ira Woodring.
 * Clears the 'screen' AKA the terminal the
 * game is being played in by printing a lot
 * of new line characters.
 ********************************************/
void clearScreen(){
	for(int i=0;i<50;i++){
		printf("\n");
	}
}

/***************************************************
 * Saves the board and information on the game
 * as a string into the passed file. Saves the
 * game in the format of:
 *
 * # //Width of board
 * # //Height of board
 * # //Amount to connect in row for win
 * # //Current player's turn
 * # //Player 1's score
 * # //Player 2's score
 * Board represented by '*' for empty spots
 * 'X' for player 1, 'O' for player 2
 * 
 * Example:
 * 7
 * 7
 * 4
 * 1
 * 0
 * 1
 * *******
 * *******
 * *******
 * *******
 * ***X***
 * X**O***
 * X**O***
 *
 * @param gs struct containing the game state
 * @param saveFile filename of file to save game
 * 				   state to
 **************************************************/
void save(struct GameState* gs, char *saveFile) {
	//Int to hold size so know how big final buff needs to be
	int totalSize = 0;
	
	char* width = intToStr(gs->width, &totalSize);
	printf("Width is: %s\n", width);
	char* height = intToStr(gs->height, &totalSize);
	printf("Height is: %s\n", height);
	char* connect = intToStr(gs->connect, &totalSize);
	printf("Connect is: %s\n", connect);
	char* currentPlayer = intToStr(gs->currentPlayer, &totalSize);
	printf("Current Player is: %s\n", currentPlayer);
	char* player1Score = intToStr(gs->score[0], &totalSize);
	printf("P1 Score is: %s\n", player1Score);
	char* player2Score = intToStr(gs->score[1], &totalSize);
	printf("P2 is: %s\n", player2Score);
	
	//(gs->width + 1) is for amount of '\n' that occur and the '\0'
	int boardSize = (int)(((gs->height * (gs->width + 1))) * sizeof(char));
	char* board = malloc(boardSize);
	if (board == NULL) {
		printf(RED "Failure to allocate memory for save.\n" RESET);
		exit(1);
	}
	//strcat changes pointer of string so need to store original to free
	char* boardStartPoint = board;
	//Must have the following line to initalize the board char array
	//or else first call to strcat will add random junk to the string
	strcpy(board, "");
	for (int i = 0; i < gs->height; i++) {
		for (int j = 0; j < gs->width; j++) {
			char temp[2];
			sprintf(temp, "%c", gs->board[j][i]);
			strcat(board, temp);
		}
		strcat(board, "\n");
	}
	
	totalSize += boardSize;
	//Holds the entire save file string
	char buffer[totalSize];
	sprintf(buffer, "%s\n%s\n%s\n%s\n%s\n%s\n%s", width, height, connect,
			currentPlayer, player1Score, player2Score, board);
	
	//Free pointers
	free(width);
	free(height);
	free(connect);
	free(currentPlayer);
	free(player1Score);
	free(player2Score);
	free(boardStartPoint);
	
	if (write_file(saveFile, buffer, totalSize) == 0) {
		printf(BOLD "\nGame saved successfully as file with name '%s'!\n" RESET, saveFile);
	} else {
		printf(RED "\n\aFile failed to save!\n" RESET);
	}
}

/**********************************************************
 * Converts passed int into a char array and returns
 * the pointer to that array. Need to know size of int
 * in terms of size of chars. If log base 10 on the int
 * and ceil function it, can get how many chars it will
 * be, so times that amount by size of char and then 
 * make it an int and that is the size needed for 
 * character array to hold the desired int. Need to do a
 * +1 to get terminating char. Don't waste resources doing
 * the math calculations if integer is less than 10 since
 * that will always be 2 chars (also because log10 of
 * anything under 2 won't work).
 * So follow format of:
 * (int)((ceil(log10(<int to convert>))+1) * sizeof(char))
 * Format from user on StackOverflow:
 * http://stackoverflow.com/a/8257728
 *
 * @param x integer to convert
 * @param sizeTotal pointer to int holding total size of
 *					all the converted strings
 * @return char* to string created
 *********************************************************/
char* intToStr(int x, int* sizeTotal) {
	int size;
	//Don't waste resources doing math if less than 10
	//Know it will be represented by 1 char + 1 char for \0
	if (x < 10) {
		size = (int)(2 * sizeof(char)); //Need 2 for char + terminal char
	} else {
		size = (int)((ceil(log10(x)) + 1) * sizeof(char));
	}
	char* str = malloc(size);
	if (str == NULL) {
		printf(RED "Failure to allocate memory for integer conversion.\n" RESET);
		exit(1);
	}
	sprintf(str, "%d", x);
	*sizeTotal += size; //Don't do -1 because want room for \n chars
	return str;
}

/*************************************************
 * Reads a saved game state and puts the saved
 * attributes into the GameState struct.
 *
 * @param gs struct containing the game state
 * @param loadFile filename for file with game 
 * 				   state to load
 ************************************************/
void load(struct GameState* gs, char *loadFile) {
	//Handle any segfaults that occur from corrupted game file
	//See loadErrorHandler() function for more info
	signal(SIGSEGV, loadErrorHandler);
	
	//Buffer to hold the load file string
	char* buffer;
	int size = read_file(loadFile, &buffer);
	//Check for read_file error
	if (size < 1) {
		loadErrorHandler();
	}
	
	//Want to seperate the buffer by \n
	char delim[2] = "\n";
	char* token;
	//Get width
	token = strtok(buffer, delim);
	gs->width = atoi(token);
	//Get height
	token = strtok(NULL, delim);
	gs->height = atoi(token);
	//Get connect
	token = strtok(NULL, delim);
	gs->connect = atoi(token);
	//Get current player
	token = strtok(NULL, delim);
	gs->currentPlayer = atoi(token);
	//Get player 1 score
	token = strtok(NULL, delim);
	gs->score[0] = atoi(token);
	//Get player 2 score
	token = strtok(NULL, delim);
	gs->score[1] = atoi(token);
	//New game with the loaded width and height
	newGame(gs);
	//Get board row by row and fill in board with
	//any pieces that were placed
	for (int i = 0; i < gs->height; i++) {
		token = strtok(NULL, delim);
		for (int j = 0; j < gs->width; j++) {
			if (*(token + j) == 'X' || *(token + j) == 'O') {
				gs->board[j][i] = *(token + j);
			}
		}
	}
	
	free(buffer);
}

/*******************************************************************
 * Used as a signal handler to handle any seg fault errors
 * that occur when trying to load a game state from a file.
 * Information on signal.h library can be found at:
 * http://pubs.opengroup.org/onlinepubs/007908775/xsh/signal.h.html
 ******************************************************************/
void loadErrorHandler() {
	printf(RED "Load error! Terminating program.\n" RESET);
	exit(1); //Use exit() since will free any allocated memory
}