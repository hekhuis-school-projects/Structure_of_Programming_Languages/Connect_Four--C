/**************************************************
 * This is a parser to be used for a connect four
 * game to get command line arguments.
 *
 * @author Kyle Hekhuis
 *************************************************/
 
#include <stdlib.h>
#include <argp.h>
#include <string.h>
#include "connect_parser.h"

/****************************************************************
 * All code related to argp.h (static char doc[],
 * static char args_doc[], static struct argp_option options[],
 * struct arguments, static error_t parse opt, and
 * static struct argp argp) is based off of Example 3 in the
 * GNU software manual for Argp.
 * https://www.gnu.org/software/libc/manual/html_node/Argp.html
 ***************************************************************/

/** Program title and version. */
const char *argp_program_version = "Connect Four Project 1.0";
 
/** Contact email for bugs. */
const char *argp_program_bug_address = "hekhuisk@mail.gvsu.edu";
 
/** Program description provided by Prof. Ira Woodring */
char doc[] = "\nConnect Four is a game where two players drop "
			 "colored tiles (represented here by 'X' for "
			 "player 1 and 'O' for player 2) into a game "
			 "board in such a way that four (or user specified"
			 " amount) similarly colored tiles line up "
			 "either horizontally, vertically, or diagonally."
			 " The first person to \"connect four\" tiles in "
			 "such a manner wins. The size of the board and "
			 "amount needed to connect to win can be specified "
			 "by the user. Use --help to see commands.";

/** Program command line usage. */
char args_doc[] = "Usage: [-w] [-h] [-s] [-c] [-l]";

/*********************************************************************
 * Struct holding command line arguments this program accepts.
 *
 * -w or -width= to set width of board
 * -h or -height= to set height of board
 * -s or -square= to set set square size of board
 * -c or -connect= to set amount needed in a row for win
 * -l or -load= to designate a file containing a game state to load
 ********************************************************************/
struct argp_option options[] = {
	{"width", 'w', "WIDTH", 0, "Set width of board to WIDTH."},
	{"height", 'h', "HEIGHT", 0, "Set height of board to HEIGHT."},
	{"square", 's', "SQUARE", 0, "Set board to square with size SQUARE."},
	{"connect", 'c', "CONNECT", 0, "Set CONNECT as number of pieces in a row needed to win."},
	{"load", 'l', "FILENAME", 0, "Load saved game state from FILENAME."},
	{0}
};

/***************************************
 * Parses a single command line option.
 **************************************/
 error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments = state->input;
	
	switch (key) {
		case 'w': //Width argument
			arguments->width = atoi(arg);
			break;
		case 'h': //Height argument
			arguments->height = atoi(arg);
			break;
		case 's': //Square argument
			arguments->square = atoi(arg);
			break;
		case 'c': //Connect argument
			arguments->connect = atoi(arg);
			break;
		case 'l': //Load argument
			arguments->loadFile = arg;
			break;
		case ARGP_KEY_ARG: return 0;
		default: return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

/********************************
 * Command line argument parser. 
 *******************************/
struct argp argp = {options, parse_opt, args_doc, doc};

/******************************************************
 * Instance of arguments struct to hold the arguments.
 *****************************************************/
struct arguments args;

/****************************************
 * Sets up variables for argument struct 
 * with passed command line arguments.
 ***************************************/
void setup(int argc, char** argv) {
	//Default values for command line arguments
	args.width = 7;
	args.height = 7;
	args.square = 7;
	args.connect = 4;
	args.loadFile = NULL;

	//Parse command line arguments
	argp_parse(&argp, argc, argv, 0, 0, &args);
	//Check if square param passed. If it has set
	//width and height to square size.
	if (args.square != 7) {
		args.width = args.square;
		args.height = args.square;
	}
	errorCheck(args.width, args.height, args.square, args.connect);
	
}

/**************************************************************
 * Checks to see if passed parameters are valid or not. Gives
 * error and exits program if one of the parameters is wrong.
 *
 * @param width width param to check
 * @param height height param to check
 * @param square square param to check
 * @param connect connect param to check
 *************************************************************/
void errorCheck(int width, int height, int square, int connect) {
	if (width < 1 || height < 1 || square < 1 || connect < 1) {
		printf("Entered in a wrong parameter value.\n"
			   "All parameters must be greater than 0.\n");
		exit(1);
	}
	//Get largest dimension of board. Connect can't be
	//greater than that.
	int max = (width > height) ? width : height;
	if (connect > max) {
		printf("Amount needed to connect for a win can't be "
			   "greater than the largest dimension of the "
			   "board.\n");
		exit(1);
	}
}