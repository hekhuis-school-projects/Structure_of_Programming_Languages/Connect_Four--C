/*****************************************************
 * This is a utilities file that can open and read a
 * file into a buffer, or it can read a buffer
 * and put its contents into a file.
 *
 * @author Kyle Hekhuis
 ****************************************************/
 
#ifndef H_FILE_UTILS
#define H_FILE_UTILS

/**************************************************************
 * Reads a text file and puts the contents of it into a
 * buffer of characters. Returns the size of the file
 * which is also the size of the buffer.
 *
 * @param filename - filename of text file to read
 * @param **buffer - pointer to buffer of characters
 * @return -1 for failure to open file, otherwise size of file
 **************************************************************/
int read_file(char* filename, char **buffer);

/*********************************************************
 * Writes a buffer of characters to a file with the name
 * of the passed filename.
 *
 * @param filename - filename of file to save to
 * @param *buffer - buffer of characters to read from
 * @param size - size of the buffer
 * @return 0 if successful
 *********************************************************/
int write_file(char* filename, char *buffer, int size);

#endif