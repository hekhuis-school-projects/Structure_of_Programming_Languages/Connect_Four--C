Connect Four
Language: C
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 343 - Structure of Programming Languages
Semester / Year: Winter 2017

This program creates a Connect Four game in the terminal.
For full specifications see 'connect 4 desc.pdf'.

Compile with either gcc or clang on Linux.

Usage:
Run the makefile and then run the ConnectFour executable

Options to pass as parameters:
'-w ' or '-width=' --Set width of board
'-h ' or '-height=' --Set height of board
'-s ' or '-square=' --Set board to square of specified size
'-c ' or '-connect=' --Set number of pieces in a row needed to win
'-l ' or '-load=' --Set saved game state to load